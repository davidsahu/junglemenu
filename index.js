class Animal {
  constructor(name, energy, food) {
    this.name = name;
    this.energy = energy;
    this.food = food;
  }

  eat() {
    console.log(`${this.name} im eating and gaining points`);
  }

  play() {
    console.log(`${this.name} i dont know what to do, i dont play`);
  }

  sleep() {
    console.log(`${this.name} im sleeping`);
  }

  sound() {
    console.log(`${this.name} im making sound`);
  }
}

class Tiger extends Animal {
  constructor(name, energy, food) {
    super(name, energy, food);
  }

  eat() {
    if (this.food === "grain") {
      console.log("I cant eat that :( remember my sensitive digestive system");
    } else {
      let newEnergy = this.energy + 5;
      console.log(`Tiger Recovering +5 energy`);
      console.log(`Tiger ${this.name} now has ${newEnergy} of energy`);
    }
  }
  sleep() {
    let newEnergy = this.energy + 5;
    console.log(
      `Tiger gains +5 of energy is sleeping, now has ${newEnergy} of energy `
    );
  }
  sound() {
    if (this.energy >= 3) {
      let newEnergy = this.energy - 3;
      console.log(`Tiger ${this.name}  say raaawr raaawr`);
      console.log(`Tiger use -3 of energy, now has ${newEnergy} of energy `);
    } else {
      console.log(
        `Tiger dont have enough energy,  has  ${newEnergy} of energy `
      );
    }
  }
}

class Monkey extends Animal {
  constructor(name, energy) {
    super(name, energy);
  }

  eat() {
    let newEnergy = this.energy + 2;
    console.log(`Monkey Recovering +2 energy`);
    console.log(`Now Mokey ${this.name} has ${newEnergy} of energy`);
  }

  sleep() {
    let newEnergy = this.energy + 10;
    console.log(
      `Monkey gains +10 is sleeping, now has ${newEnergy} of energy `
    );
  }

  play() {
    if (this.energy >= 8) {
    let newEnergy = this.energy - 8;
      console.log(`Monkey ${this.name} scream Oooo Oooo! and is playing `);
      console.log(`Mokey lost -8 of energy, now has ${newEnergy} of energy`);
    } else {
      console.log(`I’m too tired, i need rest or eat`);
    }
  }

  sound() {
    if (this.energy >= 3) {
      let newEnergy = this.energy - 3;
      console.log(`Monkey ${this.name} Oooo Oooo`);
      console.log(`Tiger use -3 of energy, now has ${newEnergy} of energy `);
    } else {
      console.log(
        `Tiger dont have enough energy,  has  ${newEnergy} of energy `
      );
    }
  }
}

let t = new Tiger("Toño", 4, "meat");
let m = new Monkey("Albert", 8);

// Tiger
// t.play();
// t.sleep();
// t.eat();
t.sound();

// Monkey
//m.play();
// m.sleep();
//m.eat();
m.sound();
